import * as React from 'react';
import {mount} from 'enzyme';

/** Render Component */
function renderComponent(ComponentClass: any, props?: any) {

  return mount(
    <ComponentClass {...props} />
  );
}

export {renderComponent};
