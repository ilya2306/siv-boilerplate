import { expect } from 'chai';
import { renderComponent } from 'helpers/TestHelper';
import { Test } from './index';

describe('<Home />', () => {

  const component = renderComponent(Test);

  it('Renders Barbar Logo', () => {
    expect(component.find('img')).to.exist;
  });

  it('Has a p element that says Hello!', () => {
    expect(component.find('p').text()).eql('Hello!');
  });

});
