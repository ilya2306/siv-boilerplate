import * as React from 'react';

interface ITestProps {
  name: string;
}

class Test extends React.Component<ITestProps, any> {
  public render() {
    return (
      <div className="home">
        <img src="https://yastatic.net/morda-logo/i/citylogos/yandex_no1-logo-ru.png" alt="Image"/>
        <p>Hello!</p>
      </div>
    );
  }
}

export {Test};
