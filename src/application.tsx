// import * as e6p from 'es6-promise';

// (e6p as any).polyfill();
// import 'isomorphic-fetch';

import * as React from 'react';
import {render} from 'react-dom';
import {Test} from 'containers';

render(
  <Test name="World"/>,
  document.getElementById('app')
);
