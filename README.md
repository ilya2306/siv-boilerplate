## Libraries

#### Core
- [TypeScript](https://www.typescriptlang.org/)
- [React](https://github.com/facebook/react) & [React DOM](https://github.com/facebook/react) for views.

#### Utilities
- [Isomorphic Fetch](https://github.com/matthew-andrews/isomorphic-fetch) with [ES6-Promise](https://github.com/stefanpenner/es6-promise) for using fetch api.

#### Build System
- [rimraf](https://github.com/isaacs/rimraf) The UNIX command rm -rf for node.
- [Webpack](https://github.com/webpack/webpack) for bundling.
  - [Awesome TypeScript Loader](https://github.com/s-panferov/awesome-typescript-loader) as ts loader.
  - [Babel Loader](https://github.com/babel/babel-loader) as js loader.
- [Sourcemap Loader](https://github.com/webpack/source-map-loader)
- [tslint Loader](https://github.com/wbuchwalter/tslint-loader) for using tslint as preloader on build process.
- [tslint react](https://github.com/palantir/tslint-react) Lint rules related to React & JSX for TSLint.
- [html-webpack-plugin](https://github.com/jantimon/html-webpack-plugin) for update html with webpack bundle.
- [webp-loader](https://github.com/kavu/webp-loader) for convert images to webp format on build process.


#### Developer Experience
- [tslint](https://github.com/palantir/tslint) for linting TypeScript files.
- [stylelint](https://github.com/stylelint/stylelint) for linting styles.
- [Chalk](https://github.com/chalk/chalk) for colored terminal logs.


## Directory Structure
```
.
├── build                       # Built, ready to serve app.
├── config                      # Root folder for configurations.
│   ├── test                    # Test configurations.
│   ├── webpack                 # Webpack configurations.
│   └── main.js                 # Generic App configurations.
│   ├── postbuild.js            # Scripts executable after any build.
│   └── utils.js                # Entry point.
├── node_modules                # Node Packages.
├── src                         # Source code.
│   ├── app                     # App folder.
│   │   ├── components          # React Components.
│   │   ├── containers          # React/Redux Containers.
│   │   ├── helpers             # Helper Functions & Components.
│   │   ├── redux               # Redux related code aka data layer of the app.
│   │   │   ├── modules         # Redux modules.
│   │   │   ├── reducers.ts     # Main reducers file to combine them.
│   │   │   └── store.ts        # Redux store, contains global app state.
│   │   └── routes.tsx          # Routes.
│   ├── styles                  # Styles folder.
│   │   ├── index.scss          # Styles entry point.
│   │   └── style.js            # Entry point for webpack style loader.
│   ├── vendor                  # Vendor folder.
│   │   └── main.ts             # Additional dependencies that will be include in bundle.
│   └── application.tsx         # Entry point.
├── .babelrc                    # Config for babel.
├── .editorconfig               # To define and maintain consistent coding styles between different editors and IDEs.
├── .gitattributes              # Define attributes per path
├── .gitignore                  # Specifies intentionally untracked files to ignore
├── .gitlab-ci.yml              # Used by GitLab Runner to manage your project's jobs.
├── .stylelintrc                # Configures stylelint.
├── package.json                # Package configuration.
├── postcss.config.js           # Postcss config
├── README.md                   # This file
├── tsconfig.json               # TypeScript transpiler configuration.
└── tslint.json                 # Configures tslint.
```

## Installation

You can clone from this repository or [install the latest version](https://gitlab.com/ilya2306/siv-boilerplate.git) as a zip file or a tarball.

```bash
$ git clone https://gitlab.com/ilya2306/siv-boilerplate.git
$ cd siv-boilerplate
$ npm install
```

## Usage

All commands defaults to development environment. You can set `NODE_ENV` to `production` or use the shortcuts below.

```bash
# Running
$ npm start             # This starts developing server and open a browser


# Building
$ npm run dev           # This builds the app in development mode
$ npm run watch         # This builds the app in development mode and will be see for changing
$ npm build             # This builds the app in production mode

# Testing
$ npm test

# Linting
$ npm run lint          # This runs linting script and styles without fix
$ npm run lint:ts       # This runs linting script
$ npm run lint:ts-fix   # This runs linting script and try fix it
$ npm run lint:styles   # This runs linting styles

```

For Windows users, recommends using the shortcuts instead of setting environment variables because they work a little different on Windows.

___

## SIV-boilerplate

You can contact me at [s_ilya@yahoo.com](mailto:s_ilya@yahoo.com)
