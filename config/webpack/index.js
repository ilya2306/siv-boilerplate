`use strict`;
const chalk = require('chalk');
const PRODUCTION = process.env.NODE_ENV === 'production' ? ' PRODUCTION' : 'DEVELOPMENT';

console.log(chalk.yellow(`
      o o o o ~~  ~~ ~              ${PRODUCTION}             _____
   o     _____         ________________ ________________ ___|_=_|_()__
 .][_mm__|[]| ,===___ ||              | |              | |          |
>(_______|__|_|_GBRR_]_|              |_|              |_|          |_|
_/oo-OOOO-oo' !oo!!oo!=\`!o!o!----!o!o!'=\`!o!o!----!o!o!'=\`!o!o--o!o!'
+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=
`));

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./prod');
} else {
  module.exports = require('./dev');
}
