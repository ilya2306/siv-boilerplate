const path = require('path');
const webpack = require('webpack');
const express = require('express');
const chalk = require('chalk');
const opn = require('opn');

const config = require('./webpack/dev');
const {port, host} = require('./main');

const app = express();
const compiler = webpack(config);

app.all('/', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With');
  next();
});

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: `http://${host}:${port}/`
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, host, (err) => {
  if (err) {
    return console.log(chalk.red(err.message));
  }

  const addr = `http://${host}:${port}`;
  console.log(chalk.green(`Listening at ${addr}`));

  opn(addr);
});