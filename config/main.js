/** General Configurations */

const {env} = process;

const config = {
  env: env.NODE_ENV || 'development',
  host: env.HOST || 'localhost',
  port: env.PORT || 8889,
  karmaPort: 9876,

  // This part goes to React
  app: {}
};

module.exports = config;