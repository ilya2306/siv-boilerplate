/** Utils for webpack tasks */

const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const rimraf = require('rimraf');

const Utils = (() => {
  const copySync = (src, dest, overwrite) => {
    const {base} = path.parse(src);
    src = path.resolve(src);
    dest = path.resolve(dest, base);

    if (overwrite && fs.existsSync(dest)) {
      fs.unlinkSync(dest);
    }

    try {
      const data = fs.readFileSync(src);
      fs.writeFileSync(dest, data);
    } catch (e) {
      console.log(chalk.red(`⚠ ${e.message}`));
    }
  };

  const createIfDoesntExist = dest => {
    if (!fs.existsSync(dest)) {
      fs.mkdirSync(dest);
    }
  };

  const clearDir = (pattern, options) => {
    rimraf.sync(pattern, {glob: options});
  };

  const remove = src => {
    if (fs.existsSync(src)) {
      fs.unlinkSync(src);
    }
  };

  return {
    copySync,
    createIfDoesntExist,
    clearDir,
    remove
  }
})();

module.exports = Utils;
