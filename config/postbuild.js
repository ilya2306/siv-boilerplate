const {createIfDoesntExist, copySync, remove} = require('./utils');
createIfDoesntExist('build');
copySync('public/manifest.json', 'build');
copySync('public/favicon.ico', 'build');

remove('build/js/style.js');
remove('build/js/style.js.map');
